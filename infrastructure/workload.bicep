param apiName string
param rgSharedResources string
param location string

param linuxFxStack string = 'NODE|18-lts'
param startupCommand string = 'npx serve -s'
param healthcheck string = '/'
param aspName string = 'asp-frontend'
param privateDnsZoneName string = 'privatelink.azurewebsites.net'
param vnetName string
param subnetName string

resource appServicePlan 'Microsoft.Web/serverfarms@2021-01-15' existing = {
  name: aspName
  scope: resourceGroup(rgSharedResources)
}

resource AppServiceApp 'Microsoft.Web/sites@2021-01-15' = {
  name: apiName
  location: location
  identity: {
    type: 'SystemAssigned'
  }
  properties: {
    serverFarmId: appServicePlan.id
    httpsOnly: true
    clientAffinityEnabled: false
    siteConfig: {
      linuxFxVersion: linuxFxStack
      appCommandLine: startupCommand
      appSettings: [
        {
          name: 'WEBSITE_WEBDEPLOY_USE_SCM'
          value: 'false'
        }
        {
          name: 'SCM_DO_BUILD_DURING_DEPLOYMENT'
          value: 'false'
        }
        {
          name: 'WEBSITE_HEALTHCHECK_MAXPINGFAILURES'
          value: '10'
        }
        {
          name: 'WEBSITE_WARMUP_PATH'
          value: healthcheck

        }
      ]
    }
  }
}

resource staging 'Microsoft.Web/sites/slots@2022-09-01' = {
  location: location
  name: 'staging'
  parent: AppServiceApp
  properties: {
    clientAffinityEnabled: false
    httpsOnly: true
  }
}

var privateEndpointName = 'pe-${apiName}'

resource privateEndpoint 'Microsoft.Network/privateEndpoints@2023-05-01' = {
  name: privateEndpointName
  location: location
  properties: {
    subnet: {
      id: resourceId(rgSharedResources,'Microsoft.Network/virtualNetworks/subnets', vnetName, subnetName)
    }
    privateLinkServiceConnections: [
      {
        name: privateEndpointName
        properties: {
          groupIds: ['sites']
          privateLinkServiceId: AppServiceApp.id
        }
      }
    ]
  }
}

module addToPrivateDns 'AddToPrivateDns.bicep' = {
  name: 'addToPrivateDns'
  params: {
    privateDnsZoneName: privateDnsZoneName
    privateEndpointName: privateEndpointName
    appResourceGroupName: resourceGroup().name
    appName: apiName
  }
  dependsOn: [privateEndpoint]
  scope: resourceGroup(rgSharedResources)
}

