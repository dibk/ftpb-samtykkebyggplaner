// Types
import { UPDATE_ATTACHMENTS_TRANSFER_COMPLETE } from "constants/types";

export const updateAttachmentsTransferComplete = (attachmentsTransferComplete) => (dispatch) => {
    dispatch({ type: UPDATE_ATTACHMENTS_TRANSFER_COMPLETE, payload: attachmentsTransferComplete });
}
