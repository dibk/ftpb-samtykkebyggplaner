// Types
import { UPDATE_CLIENT_LOGGER } from "constants/types";

export const updateClientLogger = (clientLogger) => (dispatch) => {
    dispatch({ type: UPDATE_CLIENT_LOGGER, payload: clientLogger });
};
