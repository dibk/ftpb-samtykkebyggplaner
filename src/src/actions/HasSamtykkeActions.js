import { UPDATE_HAS_SAMTYKKE } from 'constants/types';

export const updateHasSamtykke = (hasSamtykke) => (dispatch) => {
    dispatch({ type: UPDATE_HAS_SAMTYKKE, payload: hasSamtykke });
}
