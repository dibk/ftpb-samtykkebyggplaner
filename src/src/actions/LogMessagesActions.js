// Types
import { UPDATE_LOG_MESSAGES, CLEAR_LOG_MESSAGES } from "constants/types";

export const updateLogMessages = (logMessages) => (dispatch) => {
    dispatch({ type: UPDATE_LOG_MESSAGES, payload: logMessages });
};

export const addLogMessage = (logMessage) => (dispatch, getState) => {
    const logMessages = getState().logMessages;
    logMessages.push(logMessage);
    dispatch({ type: UPDATE_LOG_MESSAGES, payload: logMessages });
};

export const clearLogMessages = () => (dispatch) => {
    dispatch({ type: CLEAR_LOG_MESSAGES, payload: [] });
};
