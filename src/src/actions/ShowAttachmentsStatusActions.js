// Types
import { UPDATE_SHOW_ATTACHMENTS_STATUS_MODAL } from "constants/types";

export const updateShowAttachmentStatusModal = (showAttachmentStatusModal) => (dispatch) => {
    dispatch({ type: UPDATE_SHOW_ATTACHMENTS_STATUS_MODAL, payload: showAttachmentStatusModal });
}
