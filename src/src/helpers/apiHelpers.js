export const getXhrResponseText = (xhrResponse) => {
    if ((xhrResponse?.Type === "string" || xhrResponse?.Type === "") && !!xhrResponse?.responseText) {
        return xhrResponse.responseText;
    } else {
        return null;
    }
};
