export const scrollToTop = () => {
    document.getElementById("main-content").scrollTop = 0;
};

export const classNameArrayToClassNameString = (classNameArray) => {
    return classNameArray?.filter((className) => className)?.join(" ") || "";
};
