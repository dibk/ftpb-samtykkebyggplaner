import { UPDATE_ARCHIVE_REFERENCE } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_ARCHIVE_REFERENCE:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
