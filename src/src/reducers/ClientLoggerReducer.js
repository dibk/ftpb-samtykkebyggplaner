import { UPDATE_CLIENT_LOGGER } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_CLIENT_LOGGER:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
