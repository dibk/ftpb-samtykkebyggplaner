import { UPDATE_EMAIL_RECEIPT_IS_SENT } from 'constants/types';

const initialState = false;

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_EMAIL_RECEIPT_IS_SENT:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;
