import { UPDATE_LOADING_MESSAGE } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_LOADING_MESSAGE:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
