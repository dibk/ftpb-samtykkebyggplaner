import { UPDATE_MESSAGE_ID } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_MESSAGE_ID:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
