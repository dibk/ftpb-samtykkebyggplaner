import { FETCH_SAMTYKKE_TILTAKSHAVER } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_SAMTYKKE_TILTAKSHAVER:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
