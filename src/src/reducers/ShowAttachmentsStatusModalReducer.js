import { UPDATE_SHOW_ATTACHMENTS_STATUS_MODAL } from 'constants/types';

const initialState = false;

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_SHOW_ATTACHMENTS_STATUS_MODAL:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;
