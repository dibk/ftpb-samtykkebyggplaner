import { FETCH_TILTAKSHAVER_SIGNATUR_XML } from 'constants/types';

const initialState = null;

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TILTAKSHAVER_SIGNATUR_XML:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;
